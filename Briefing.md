#Briefing & Analyse
Maak een responsive mobile-first webapplicatie waarin minimaal 3 datasets (1x groep) - 6 datasets (2x groep) - 9 datasets (3x groep), afkomstig uit de dataset-pool van de Worldbank, verwerkt zijn. Conceptueel denken is heel belangrijk in deze applicatie. Dit betekent dat jullie verbanden moeten leggen tussen deze datasets. Het is louter niet alleen het oplijsten en visualiseren van deze datasets, er moet een concept rond gebouwd worden.

Deze applicatie is bestemd voor +18 jarigen waarbij de gebruiksvriendelijkheid, interactiviteit, uitbreidbaarheid en schaalbaarheid van belang zijn.

---------

#Functionele Specificaties
* One Page Webapplication
    * Bookmark routes
    * of een webapplicatie bestaande uit verschillende pagina's
* Preloaders/Loaders
    * Om de app in te laden
    * Tijdens het inladen van JSON(P)
    * Lokale data bestanden
* De meeste inhoud wordt beheerd in data bastanden en dynamisch ingeladen
* Adaptive images, video's en sounds
* Google Maps integratie of gelijkaardig
    * Custom look-and-feel
* GEO-location
    * Toon de locatie van de gebruiker op een Map
    * Hou rekening met deze locatie in andere onderdelen van deze app
* Social Media Bookmarking (Open Graph)
* Animaties via SVG en/of Canvas
* Lokaal caching van data en bronbestanden (cache manifest)
* Gebruiker ervaart een interactief webapplicatie
* Gebruiker kan favoriete data lokaal bewaren
* Gebruiker kan de webapplicatie bookmarken in browser, bureaublad en als native app in het overzicht
* Automation verplicht!
    * Componenten worden via Bower toegevoegd in de components folder van de app folder
    SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
    CSS-bestanden worden met elkaar verbonden in één bestand en geminified
    De JS code wordt automatisch nagekeken op syntax fouten JS-bestanden worden met elkaar verbonden in één bestand en geminified
    De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt of Gulp
    Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom, Casper of andere bibliotheken
    
------

#Technische Specificaties

* Core technologies: HTML5, CSS3 en JavaScript
* Template engines: Jade, Haml, Swig of Handlebars
* Storage: JSON bestanden, localstorage en/of IndexedDB
* Bibliotheken: jQuery, underscore.js, lodash.js, crossroads.js, js-signals, Hasher.js, ...
* Andere bibliotheken worden hierin aangevuld tijdens het semester!
* Uitzonderingen mogelijk betreffende JavaScript bibliotheken mogelijk mits toelating!

------

#Persona's
![persona](images/Persona.png)
#ideeenborden
![ideeenborden](images/Ideeënbord.png)
#moodboards
![moodboard](images/Moodboard.png)
#Sitemap
![sitemap](images/Sitemap.png)
#wireframes
![wireframesmobiel](images/MobielWireframes.png)
![wireframesdesktop](images/wireframeDesktop.png)
#Style Tile
![Style-tile](images/Style_Tile.png)
#Visual Designs
![visualdesktop](images/ScreendesignDesktop.png)
![visualmobiel](images/MobileVisuals.png)
#Screenshot eindresultaat
![screenshot_320](screenshot_320.png)
![screenshot_480](screenshot_480.png)
![screenshot_640](screenshot_640.png)
![screenshot_960](screenshot_960.png)
![screenshot_1024](screenshot_1024.png)
